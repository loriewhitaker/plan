## Lorie's current OKRs

----

Overarching goal: 


## How this document works

---

My OKRs work as a tool to track my personal/career development at GitLab. Some important things to keep in mind:
- This document helps me strategize short-term efforts for my career development
- For the sake of transparency, my personal OKRs are visible to everyone at the company
- The OKRs listed below align with both [Staff UX Researcher](https://about.gitlab.com/job-families/engineering/ux-researcher/) and [Research Manager](https://about.gitlab.com/job-families/engineering/ux-research-manager/) job descriptions.

---

FY21-Q4 / FY22-Q1 OKRs
- Q4: Nov 1 through Jan 31 | Milestones 
- Q1: Feb 1 through April 30 | Milestones

### OKR:
Objective: Increase domain and experitise of product, while being able to guide and collaborate ux research decisions.

#### Key Results
1. [ ] 


<details>
  <summary> :star: Click to see which responsibilities algin with this OKR</summary>
  
  1. A numbered
  2. list
</details>




### OKR:
Objective:

#### Key Results
1. [ ] 

<details>
  <summary> :star: Click to see which responsibilities algin with this OKR</summary>
  
  1. A numbered
  2. list
</details>


