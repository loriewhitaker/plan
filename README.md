# Plan

This project is a collection of what I'm working on.

### Who's Lorie?

My name is Lorie Whitaker and I am a Staff UX Researcher for Ops at GitLab. You can read more about me in my [ReadMe](https://gitlab.com/loriewhitaker/readme) file.

* Current Location: Irving, Texas, USA
* Timezone: [Central Time](https://time.is/Irving)
